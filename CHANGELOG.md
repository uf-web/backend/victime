# CHANGELOG

<!--- next entry here -->

## 1.1.1
2020-05-26

### Fixes

- **validation:** Add validator on email and phone number (16b86b1afbb04dd05524daf3cf6eaefbcf5715c8)

## 1.1.0
2020-05-26

### Features

- **personnel:** Add phone number for firebase compatibility (da0bd698c84b983e855cb9da8c6fb2068e10a1e2)

## 1.0.16
2020-05-26

### Fixes

- **db:** increase timeout time (c33565d0274d7897eef2066caa989b15fab00abd)

## 1.0.15
2020-05-26

### Fixes

- **discovery:** Fix config for connection (4424a663511ad6688004caaf50331b56a274609c)

## 1.0.14
2020-05-26

### Fixes

- **discovery:** Fix missing port on Eureka URL (4f2703a4fcfcf3bcae2579a091b6773e97cda294)
- **discovery:** Change zone URL (8a67a9c2ab22f439af6a2e6ed26e36849b4c6d64)

## 1.0.13
2020-05-25

### Fixes

- **env vars:** Change Eureka env var (2b6edeb7c8324d044fd12a5b08964ba53ea7a8f8)

## 1.0.12
2020-05-19

### Fixes

- **versioning:** Repair versioning in POM (115b2b5784b8219bc6e3fdfe0826855b6057fb44)

## 1.0.11
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (9f7ebbd25152471aed613a0a23a268205cbbd400)

## 1.0.10
2020-05-18

### Fixes

- **cors:** CORS for swagger ui (f09d2e493e3d3ccec74f71be6a743f07f335f26d)
- **cors:** CORS for swagger ui (aecbf89d8cc5caecca7adaa3a4aeef46a068d84a)

## 1.0.9
2020-05-12

### Fixes

- **routes:** repair routes in controller (91173ac67a5d819ed6c559d0b79728ba28c752da)

## 1.0.8
2020-05-11

### Fixes

- **todo:** Cleanup TODO (5cfbb7f4b084d1a83aed1dfed68549fa08109e90)

## 1.0.7
2020-05-11

### Fixes

- **controller:** clean routes (dca6957f74f89b9c6e2e13093452afbf000c70e5)
- **monitoring:** Expose all actuator routes (e94425db05c32e401e83909a98a6a69eb6c84274)

## 1.0.6
2020-05-09

### Fixes

- **pom:** Update dependencies (0fc878aa9fb00fa5a3f6f1725350e86dc6d2838f)

## 1.0.5
2020-05-09

### Fixes

- **deployment:** Change registry url (5f9de336b043345f33b6a2d4f3171bf9207c0612)

## 1.0.4
2020-05-09

### Fixes

- **config:** Add discovery config (59fb7d376def36c86f562822b6e729a68ad0bcd1)

## 1.0.3
2020-05-09

### Fixes

- **var:** Update env variables (f62a719fc4603bc30674284bcc06369d27c70dd8)

## 1.0.2
2020-05-08

### Fixes

- **spring boot:** Update Spring Boot (d00145ddd7b4934f95e7566414612f6efcadc759)

## 1.0.1
2020-05-03

### Fixes

- **ci:** Add versionning in pom file (0ce8eccd3c8fbc9c969c364c8d55fcf25aa39d91)

## 1.0.0
2020-05-03

### Fixes

- **ci:** Add versionning in pom file (f0597e9724f55338a50774c261813edbe03ff87d)
- **ci:** Add versionning in pom file (c8941dab9f062d910b039fb803fde249f7fb16b6)
- **ci:** Add versionning in pom file (17c005cf9926c22e004784c943fc873a6e568ab8)

## 0.12.1
2020-04-14

### Fixes

- **ci:** Fix maven deploy (5235dc7399da9c2c5b925f4c87b690349f28284c)

## 0.12.0
2020-04-14

### Features

- **release:** Add store maven build in gitlab (9d298808dafb6888f0d0bd3881371d2d81a08a6b)

### Fixes

- **prod:** Change db timeout (a6afc6629d47b4510890e0a095b7daed5c9bc3bf)

## 0.11.2
2020-04-13

### Fixes

- **deployment:** Update running deployment with ci (caaf87c80dfd007081f4e6d3e27f4b9b44feef52)

## 0.11.1
2020-04-12

### Fixes

- **teapot:** Add quotes on code name (67d29d40f8d88b7075956d003a3df3fa744b1bc3)
- **model:** Add ToString on models (6e562137b1630f2810b3baad07251b021b7c057f)

## 0.11.0
2020-04-12

### Features

- **joke:** Add 418 (57d4aa702d49a6f8f1c8cef064120be505c5f19d)

## 0.10.1
2020-04-12

### Fixes

- **glasgow:** Fix calcul method (31ebfb7d1ef93b896106fa90a8a1b2a01cd4e3a8)
- **model:** Repair relation in json (9228d7a9b1aef10f1b5f08fb3e68a79157fe69c5)
- **controller:** Fix request type (fdfaddf6376c888f72229dd2842523db2d012e5d)
- **repository:** add find by bilan (acc0758608b715ff330f132e758c0aa3b2506ba6)
- **service:** Fix update and get methods (7b6d02c542a05d0aa2de6fa1778af1665b0ba27e)

## 0.10.0
2020-04-12

### Features

- **func:** Add Surveillance in CRUD (8ba77a4d70a690ae020359dff38ca52cc053179a)
- **surveillance:** Add Surveillance delete (319125909fcc15cc671e71a467ccfef897bf2365)
- **surveillance:** Delete all Surveillances on Victime (a9db89ca304ee4dc0ae3df88b23e915c6af57bc7)

### Fixes

- **controller:** Repair PUT methods (bf3b658d7cf0d8b1fbc86647ef0765b024b2f562)
- **service:** repair DELETE and PUT (41cb2138f1e9195ea4ff943aefcb3c44c6608b2c)
- **date:** Define json date pattern (66e4bd187cc217424f6968c5316a42393ac8bf25)

## 0.9.0
2020-04-11

### Features

- **model:** Update victime's models (883fcb2a613f303e1e3c046a64d2a243b9d6f14b)
- **calcul:** Add glasgow Calc (54c0150e4f85e81c9249d0a356e16156ac3ddea9)
- **calcul:** Add glasgow Calc (2254018a1433955885a376fa32e64ab6c32f5e9c)
- **bilan:** Add Surveillance with relation on Physique (505370a538ff5176ad744132d67b5eefda1e6686)

### Fixes

- **conf:** Disable dev tools on prod (9577fafe5c186676ad8688b12b06dba49e7edef0)
- **surveillance:** Prepare for Surveillance function (8ff8ac64b6caa247333bc88ec14a1581867b5ef0)

## 0.8.0
2020-04-10

### Features

- **deployment:** Update image on ci (80578c511f658abf8819e1df6c7af9bd081a5b59)

## 0.7.0
2020-04-09

### Features

- **victime:** Finish All CRUD (3949deff5aa8d6f4b74ea5577d6c01e087c7ed0e)

### Fixes

- **model:** Beauty (2641b03cc85e7c8a67d24498befd8aa660cb6132)
- **relation:** delete orphan (c3e468347c67430bc3cc9c5aac7b01941355ba31)

## 0.6.0
2020-04-08

### Features

- **model:** delete centre (ef9e331af5e3baa9ba0c1ee1a982549bf3bc5510)

## 0.5.0
2020-04-08

### Features

- **search:** Add search capacity (440e6a840a2fe70da412ea292377d767add6bc8d)
- **service:** Add blank impl (e17393d28acd077171a4e8f81d2d3263e9cddd1d)

### Fixes

- **relations:** relations fetch lazy (a8a88656a89da938563a3a310a8bbe72a353248e)

## 0.4.0
2020-04-07

### Features

- **model victime:** Finish first version of victime's model (5f2e2274e6b49b6be825a9a58796f1813280eee4)

## 0.3.0
2020-04-05

### Features

- **victime:** Add Victime Model (46f86ca24b847694b1acb5f1b5751d9f820318bb)

## 0.2.0
2020-04-05

### Features

- **doc:** Add springDoc (f4ad07a6780657a7b5dcece3ae392530bdbd9409)

## 0.1.1
2020-04-04

### Fixes

- **ci:** Change image for version stage (9448e881d2001ce57e2a8d89397de41184262288)
- **deployment:** Add namespace in deployment (f244f84e371cfe27401a723435d2f58c1b97efbe)
- **model:** Follow modifications on other micro-services (fbe9a4dfe1407650530ca1b479909e4f3db218b4)
- **deployment:** Use latest image (8c21fb15d4dec0b3737963dbb6ccf937aa5b470e)
- **ci:** Update container image in gitlab ci (ffebddc6ef7141b1277c54ce58c15aa13e99f5a3)

## 0.1.0
2020-03-31

### Features

- **init:** init submodule (8cc331d8683cd0b3ef93f6e1835ac1d10a2b8b2b)
- **ci:** Preapre micro-service for gitlab ci (b84971cf288512738947b85e0e00a753d92c6750)

### Fixes

- Follow other micro-services modifications (958821a1bac81718d5aa735e2ca8fbc31e8e5776)
- **ci:** Change image for version stage (9448e881d2001ce57e2a8d89397de41184262288)
- **deployment:** Add namespace in deployment (f244f84e371cfe27401a723435d2f58c1b97efbe)
- **model:** Follow modifications on other micro-services (fbe9a4dfe1407650530ca1b479909e4f3db218b4)
- **deployment:** Use latest image (8c21fb15d4dec0b3737963dbb6ccf937aa5b470e)

## 0.1.0
2020-03-24

### Features

- **init:** init submodule (8cc331d8683cd0b3ef93f6e1835ac1d10a2b8b2b)
- **ci:** Preapre micro-service for gitlab ci (b84971cf288512738947b85e0e00a753d92c6750)

### Fixes

- Follow other micro-services modifications (958821a1bac81718d5aa735e2ca8fbc31e8e5776)
- **ci:** Change image for version stage (9448e881d2001ce57e2a8d89397de41184262288)
- **deployment:** Add namespace in deployment (f244f84e371cfe27401a723435d2f58c1b97efbe)

## 0.1.0
2020-03-23

### Features

- **init:** init submodule (8cc331d8683cd0b3ef93f6e1835ac1d10a2b8b2b)
- **ci:** Preapre micro-service for gitlab ci (b84971cf288512738947b85e0e00a753d92c6750)

### Fixes

- Follow other micro-services modifications (958821a1bac81718d5aa735e2ca8fbc31e8e5776)