package com.al.victime.repository;

import com.al.victime.model.Equipe;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

@Repository
@Table("equipe")
public interface EquipeRepository extends JpaRepository<Equipe, Integer>, JpaSpecificationExecutor<Equipe> {

    Equipe findEquipeById(Integer id);

    Page<Equipe> getEquipesByPosteId(Integer posteId, Pageable pageable);

}
