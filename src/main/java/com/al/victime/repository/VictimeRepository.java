package com.al.victime.repository;

import com.al.victime.model.Equipe;
import com.al.victime.model.Poste;
import com.al.victime.model.Victime;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
@Table("victime")
public interface VictimeRepository extends JpaRepository<Victime,Long>, JpaSpecificationExecutor<Victime> {
    List<Victime> getVictimesByPoste(Poste poste);
    Page<Victime> findVictimesByPoste(Poste poste, Pageable pageable);
    Page<Victime> findVictimesByPosteAndEquipe(Poste poste, Equipe equipe, Pageable pageable);
}
