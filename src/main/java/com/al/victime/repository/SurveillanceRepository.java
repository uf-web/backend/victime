package com.al.victime.repository;

import com.al.victime.model.bilan.Physique;
import com.al.victime.model.bilan.Surveillance;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SurveillanceRepository extends JpaRepository<Surveillance, Integer>, JpaSpecificationExecutor<Surveillance> {
    Page<Surveillance> findSurveillancesByBilan(Physique bilan, Pageable pageable);
    List<Surveillance> getSurveillancesByBilan(Physique bilan);
}
