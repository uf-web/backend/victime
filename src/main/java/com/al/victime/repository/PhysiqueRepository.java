package com.al.victime.repository;

import com.al.victime.model.Victime;
import com.al.victime.model.bilan.Physique;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
@Table("bilan_physique")
public interface PhysiqueRepository extends JpaRepository<Physique,Long>, JpaSpecificationExecutor<Physique> {
    Optional<Physique> findPhysiqueByVictime(Victime victime);
}
