package com.al.victime.service;

import com.al.victime.model.Victime;
import com.al.victime.model.bilan.Physique;
import com.al.victime.model.bilan.Psy;
import com.al.victime.model.bilan.Surveillance;
import javassist.NotFoundException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public interface VictimeService {

    /**
     * Création d’une Victime
     *
     * @param idPoste id du Poste
     * @param idEquipe id de l’Equipe
     * @param victime Victime à créer
     * @return Nouvelle Victime
     */
    Optional<Victime> createVictime(Integer idPoste, Integer idEquipe, Victime victime) throws NotFoundException;

    /**
     * Ajout d'un surveillance
     *
     * @param idVictime id de la victime à laquelle ajouter la surveillance
     * @param surveillance Surveillance à ajouter
     * @return Optional Surveillance
     */
    Optional<Surveillance> addSurveillance(Long idVictime, Surveillance surveillance);

    /**
     * Récupération d’une Victime
     * @param idVictime id de la Victime
     * @return Optional Victime
     */
    Optional<Victime> getVictime(Long idVictime);

    /**
     * Récupération des Victimes d’un Poste
     * @param idPoste id du Poste
     * @param pageable pagination
     * @return Page de Victimes
     */
    Page<Victime> getVictimesOnPoste(Integer idPoste, Pageable pageable);

    /**
     * Récupération des Victimes d’une Equipe
     * @param idPoste id du Poste
     * @param idEquipe id de l’Equipe
     * @param pageable pagination
     * @return Page de Victimes
     */
    Page<Victime> getVictimesOnEquipe(Integer idPoste, Integer idEquipe, Pageable pageable);

    /**
     * Récupération d'une Surveillance
     * @param idSurveillance id de la Surveillance
     * @return Optional Surveillance
     */
    Optional<Surveillance> getSurveillance(Integer idSurveillance);

    /**
     * Récupération du bilan Physique d'une Victime
     * @param idVictime id de la Victime
     * @return Optional Physique
     */
    Optional<Physique> getBilanPhysique(Long idVictime);

    /**
     * Récupération du bilan Psy d'une Victime
     * @param idVictime id de la Victime
     * @return Optional Psy
     */
    Optional<Psy> getBilanPsy(Long idVictime);

    /**
     * List brut de toutes les Victimes du Poste
     * @param idPoste id du Poste
     * @return List de Victimes
     */
    List<Victime> listVictimesOnPoste(Integer idPoste);

    /**
     * List des Surveillances d'une Victime
     * @param idVictime id de la Victime surveillée
     * @param pageable pagination
     * @return Page de Surveillance
     */
    Page<Surveillance> listSurveillances(Long idVictime, Pageable pageable);

    /**
     * Mise à jour d’une Victime
     * @param victime Victime mise à jour
     * @param idVictime id de la Victime à mettre à jour
     * @return Victime mise à jour
     */
    Optional<Victime> updateVictime(Victime victime, Long idVictime);

    /**
     * Mise à jour du Bilan Physique d’une Victime
     * @param bilan bilan Physique
     * @param idVictime id de la Victime / id du bilan
     * @return bilan Physique modifié
     */
    Optional<Physique> updateBilanPhysique(Physique bilan, Long idVictime);

    /**
     * Mise à jour du Bilan Psy d’une Victime
     * @param bilan bilan Psy
     * @param idVictime id de la Victime / id du bilan
     * @return bilan Physique modifié
     */
    Optional<Psy> updateBilanPsy(Psy bilan, Long idVictime);

    /**
     * Mise à jour d'une Surveillance
     * @param surveillance Mise à jour de la Surveillance
     * @param idSurveillance id de la Surveillance à mettre à jour
     * @return Surveillance mise à jour
     */
    Optional<Surveillance> updateSurveillance(Surveillance surveillance, Integer idSurveillance);

    /**
     * Suppression d’une Victime
     * @param idVictime id de la Victime à supprimer
     * @return Booléen false si réussi
     */
    Boolean deleteVictime(Long idVictime);

    /**
     * @param idSurveillance id de la Surveillance à supprimer
     * @return Booléen false si réussi
     */
    Boolean deleteSurveillance(Integer idSurveillance);

    /**
     * Suppression de toutes les surveillances d'une victime
     * @param idVictime id de la Victime
     * @return Booléen false si réussi
     */
    Boolean deleteSurveillances(Long idVictime);

    /**
     * Recherche d’une Victime
     * @param specs spécifications pour la recherche
     * @param pageable pagination
     * @return page de victimes
     */
    Page<Victime> searchVictime(Specification<Victime> specs, Pageable pageable);
}
