package com.al.victime.model.bilan;

/**
 * Verbalisation
 */
public enum Verbalisation {
    /**
     * S : Spontanée
     * P : Provoquée
     * A : Absente
     */
    S,P,A
}
