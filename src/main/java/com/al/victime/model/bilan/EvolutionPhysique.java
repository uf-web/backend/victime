package com.al.victime.model.bilan;

/**
 * Enum pour l'évolution d'une Victime pendant une surveillance
 * AM : Amélioration
 * IN : Inchangé
 * AG : Aggravation
 * DC : Décèsu
 */
public enum EvolutionPhysique {
    AM,IN,AG,DC
}
