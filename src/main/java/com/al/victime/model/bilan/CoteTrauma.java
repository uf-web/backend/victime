package com.al.victime.model.bilan;

public enum CoteTrauma {
    Droit, Gauche, Deux, Avant, Arriere
}
