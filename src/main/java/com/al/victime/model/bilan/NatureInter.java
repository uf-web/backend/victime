package com.al.victime.model.bilan;

public enum NatureInter {
    Accident, Maladie, Malaise, Intoxication, Noyade, Autre
}
