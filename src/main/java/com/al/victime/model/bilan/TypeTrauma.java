package com.al.victime.model.bilan;

public enum TypeTrauma {
    Plaie, Brulure, Hemorragie, Deformation, Douleur, FractureO, FractureF
}
