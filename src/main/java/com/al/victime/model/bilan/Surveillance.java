package com.al.victime.model.bilan;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.vladmihalcea.hibernate.type.basic.PostgreSQLEnumType;
import lombok.*;
import lombok.experimental.FieldDefaults;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalTime;

@Entity
@Table(name = "surveillance")
@FieldDefaults(level= AccessLevel.PRIVATE)
@Getter @Setter
@ToString(of = {
        "id",
        "heure"
})
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@TypeDef(
        name = "pgsql_enum",
        typeClass = PostgreSQLEnumType.class
)
public class Surveillance {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;

    @Enumerated(EnumType.STRING)
    @Type( type = "pgsql_enum" )
    EvolutionPhysique evolution;

    @Column(name = "heure", columnDefinition = "TIME")
    LocalTime heure;

    String frequenceRespiratoire, SPO2, frequenceCardiaque, tensionArterielle, echelleDouleur, temperature, glycemie;

    @EqualsAndHashCode.Exclude
    @JsonBackReference(value = "surveillance")
    @ManyToOne
    @JoinColumn(name = "physique_id")
    Physique bilan;
}
