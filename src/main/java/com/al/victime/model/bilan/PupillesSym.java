package com.al.victime.model.bilan;

/**
 * Symétrie des pupilles
 */
public enum PupillesSym {
    /**
     * SYM : Symétriques
     * DSG : Droite plus grande que la Gauche
     * GSD : Gauche plus grande que la Droite
     */
    SYM,DSG,GSD
}
