package com.al.victime.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import lombok.experimental.FieldDefaults;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name = "equipe")
@FieldDefaults(level = AccessLevel.PRIVATE)
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@EqualsAndHashCode
public class Equipe {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    Integer id;

    @Column(name = "nom")
    String nom;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "chef_equipe", unique = true)
    @JsonBackReference(value = "ChefEquipeForEquipe")
    Personnel chefEquipe;

    @NonNull
    @Column(name = "binome_equipe")
    Boolean binomeEquipe;

    @Column(name = "identifiant_radio_equipe")
    String identifiantRadioEquipe;

    @Column(name = "localisation")
    String localisation;

    @ManyToOne
    @JoinColumn(name = "poste_id")
    @JsonBackReference(value = "EquipesForPoste")
    Poste poste;

    @OneToMany(mappedBy = "equipe", orphanRemoval = true, cascade = CascadeType.REMOVE)
    @JsonManagedReference(value = "PersonnelsForEquipe")
    Set<Members> personnels;

    @JsonManagedReference(value = "InterventionsForEquipe")
    @OneToMany(mappedBy="equipe")
    Set<Victime> interventions;

    @Override
    public String toString() {
        String be = binomeEquipe ? "Binôme" : "Equipe";
        return "Equipe{" +
                "nom='" + nom + '\'' +
                ", " + be +
                ", identifiantRadioEquipe='" + identifiantRadioEquipe + '\'' +
                '}';
    }

    public Integer getPosteId() {
        return this.getPoste().getId();
    }
}
